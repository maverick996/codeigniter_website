<?php

class User_model extends CI_Model {
	public function login($email, $password) {
		//$this->db->select('ime, prezime');
		$this->db->from('korisnik');
		$this->db->where('email', $email);
		$this->db->where('password', MD5($password));
		$this->db->limit(1);
		$query = $this->db->get();

		if($query)
			return $query->row();
		else
			return FALSE;
	}

	public function register($ime, $prezime, $email, $password) {
		$query = array(
		//	<ime_kol> => <value>
			'ime' => $ime,
			'prezime' => $prezime,
			'email' => $email,
			'password' => MD5($password)
		);

		$this->db->insert('korisnik', $query);
		if($this->db->affected_rows() > 0)
			return TRUE;
		return FALSE;
	}
}
 ?>
