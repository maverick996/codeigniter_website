<div id="container">
	<h1>Dobrodošli na naš sajt</h1>
	<div id="body">
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		<?php if($logged_in): ?>
			Ulogovan si kao <?php echo $user_name . ' ' . $user_last; ?>
			<div>
				<a href="<?php echo base_url('user/logout') ?>">Odjavi se</a>
			</div>
		<?php else: ?>
			<h2><a href="<?php echo base_url('user') ?>">Prijavi se</a></h2>
		<?php endif ?>
	</div>
</div>
