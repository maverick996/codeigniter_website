<div id="container">
	<h1>Registruj se</h1>
	<div id="body">
		<?php echo validation_errors(); ?>
		<?php echo form_open(base_url('user/register')) ?>
			<div>
				<label for="ime">Ime: </label>
				<input type="text" name="ime" id="ime">
			</div>
			<div>
				<label for="prezime">Prezime: </label>
				<input type="text" name="prezime" id="prezime">
			</div>
			<div>
				<label for="email">Email: </label>
				<input type="email" name="email" id="email">
			</div>

			<div>
				<label for="pw">Password: </label>
				<input type="password" name="password" id="pw">
			</div>
			<button type="submit" name="button">Registruj se</button>
		<?php echo form_close() ?>
	</div>
</div>
