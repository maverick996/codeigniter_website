<div id="container">
	<h1>Prijavi se</h1>
	<div id="body">
		<?php echo validation_errors(); ?>
		<?php echo form_open(base_url('user')) ?>
			<div>
				<label for="email">Email: </label>
				<input type="email" name="email" id="email">
			</div>

			<div>
				<label for="pw">Password: </label>
				<input type="password" name="password" id="pw">
			</div>
			<button type="submit" name="button">Prijavi se</button>
		<?php echo form_close() ?>
		<p>Ako niste korisnik na nasem sajtu slobodno se prijavite: <a href="<?php echo base_url('user/register') ?>">Prijavi se</a></p>
	</div>
</div>
