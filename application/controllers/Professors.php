<?php

class Professors extends CI_Controller {
	public function __construct() {
		parent::__construct(); // Eksplicitno pozivanje kontstruktora
		$this->load->model('Professors_model', 'prof');
	}

	public function index() {
		$data['naslov'] = "Profesori";
		$data['opis'] = "Lista profesora 123";

		$data['profesori'] = $this->prof->getProfessors();

		$this->load->view('template\header', $data);
		$this->load->view('profesori\lista', $data);
		$this->load->view('template\footer');
	}
}

 ?>
