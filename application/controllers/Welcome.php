<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index() {
		$data['naslov'] = "Početna";
		$data['logged_in'] = false;

		if($this->session->userdata('prijava')) {
			$data['logged_in'] = true;
			$data['user_name'] = $this->session->prijava['ime'];
			$data['user_last'] = $this->session->prijava['prezime'];
		}

		$this->load->view('template/header', $data);
		$this->load->view('welcome_message', $data);
		$this->load->view('template/footer');
	}
}
