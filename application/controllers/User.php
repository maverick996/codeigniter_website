<?php

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('User_model', 'user');
	}

	public function index() {
		$data['naslov'] = "Login";

		$this->form_validation->set_rules('email', 'eMail',
		'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Lozinka',
		'trim|required|callback_check_user');

		if($this->form_validation->run() == TRUE) {
			redirect(base_url(), 'refresh');
		}

		if($this->session->userdata('prijava')) {
			redirect(base_url(), 'refresh');
		}

		$this->load->view('template/header', $data);
		$this->load->view('user/login');
		$this->load->view('template/footer');
	}

	public function check_user($password) {
		$email = $this->input->post('email');
		$result = $this->user->login($email, $password);

		if($result) {
			$session = array(
				'id' => $result->korisnikID,
				'ime' => $result->ime,
				'prezime'=> $result->prezime
			);

			$this->session->set_userdata('prijava', $session);
			return TRUE;
		} else {
			$this->form_validation->set_message('check_user', 'Nema vas u bazi');
			return FALSE;
		}
	}

	public function register() {
		$data['naslov'] = "Registracija";

		$this->form_validation->set_rules('ime', 'Ime', 'trim|required');
		$this->form_validation->set_rules('prezime', 'Prezime', 'trim|required');
		$this->form_validation->set_rules('email', 'eMail', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Lozinka', 'trim|required');

		if($this->form_validation->run() == TRUE) {
			$ime = $this->input->post('ime');
			$prezime = $this->input->post('prezime');
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			if($this->user->register($ime, $prezime, $email, $password)) {
				redirect(base_url(), 'refresh');
			}
		}

		if($this->session->userdata('prijava')) {
			redirect(base_url(), 'refresh');
		}

		$this->load->view('template/header', $data);
		$this->load->view('user/register');
		$this->load->view('template/footer');
	}

	public function logout() {
		$this->session->unset_userdata('prijava');
		session_destroy();
		redirect(base_url(), 'refresh');
	}
}

 ?>
